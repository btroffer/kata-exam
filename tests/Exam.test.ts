import {QuestionMultipleChoice} from "../src/QuestionMultipleChoice";
import {QuestionFreeText} from "../src/QuestionFreeText";
import {Answer} from "../src/answer";
import {Exam} from "../src/exam";

describe('Test on `Exam`', () => {
  test('When an exam is created it has no questions', () => {
    const exam = new Exam()

    expect(exam.hasQuestions()).toBeFalsy()
    expect(exam.amountCorrectQuestions()).toBe(0)
  });

  test('You can  answer a question correctly', () => {
    const exam = new Exam()
    const question = new QuestionFreeText('Pregunta 1', 'Respuesta valida pregunta 1');
    exam.addQuestion(question)
    exam.answerQuestion(question, 'Respuesta valida pregunta 1')

    expect(exam.hasQuestions()).toBeTruthy()
    expect(exam.amountQuestions()).toBe(1)
    expect(exam.amountCorrectQuestions()).toBe(1)
  });

  test('You can  answer a question incorrectly', () => {
    const exam = new Exam()
    const question = new QuestionFreeText('Pregunta 1', 'Respuesta valida pregunta 1');
    exam.addQuestion(question)
    exam.answerQuestion(question, 'Respuesta invalida pregunta 1')

    expect(exam.hasQuestions()).toBeTruthy()
    expect(exam.amountQuestions()).toBe(1)
    expect(exam.amountCorrectQuestions()).toBe(0)
  });

  test('I could not answer a question that is not on the exam', () => {
    const exam = new Exam()
    const questionNotExist = new QuestionFreeText('Pregunta Not exist', 'Respuesta valida pregunta 1');

    expect(
      () =>exam.answerQuestion(questionNotExist, 'Respuesta invalida pregunta 1')
    ).toThrow(Exam.ERROR_ANSWER_NOT_FOUND)
    expect(exam.hasQuestions()).toBeFalsy()
  });

  test('Cannot add duplicate questions', () => {
    const exam = new Exam()
    const question = new QuestionFreeText('Pregunta 1', 'Respuesta valida pregunta 1')
    exam.addQuestion(question)

    expect(
      () => exam.addQuestion(question)
    ).toThrow(Exam.ERROR_DUPLICATED_QUESTION)
    expect(exam.hasQuestions()).toBeTruthy()
    expect(exam.amountQuestions()).toBe(1)
  });

  test('Multiple choice questions added', () => {
    const exam = new Exam()
    const question1 = new QuestionMultipleChoice ('Pregunta 1', ['option 1', 'option 2', 'option 3'], 'option 2');
    exam.addQuestion(question1)
    exam.answerQuestion(question1, 'option 2')

    expect(exam.hasQuestions()).toBeTruthy()
    expect(exam.amountQuestions()).toBe(1)
    expect(exam.amountCorrectQuestions()).toBe(1)
  })

  test('Multiple choice and free text questions are added', () => {
    const exam = new Exam()
    const question1 = new QuestionMultipleChoice ('Pregunta multiple choice ', ['option 1', 'option 2', 'option 3'], 'option 2');
    const question = new QuestionFreeText('Pregunta free text', 'Respuesta valida pregunta 1');
    exam.addQuestion(question1)
    exam.addQuestion(question)
    exam.answerQuestion(question1, 'option 2')
    exam.answerQuestion(question, 'Respuesta valida pregunta 1')

    expect(exam.hasQuestions()).toBeTruthy()
    expect(exam.amountQuestions()).toBe(2)
    expect(exam.amountCorrectQuestions()).toBe(2)
  })

  test('Correct Answer of a multiple choice question has to be among the options', () => {
    expect(
      () => new QuestionMultipleChoice ('Pregunta multiple choice ', ['option 1', 'option 2', 'option 3'], 'invalid option 2')
    ).toThrow(QuestionMultipleChoice.ERROR_MULTIPLE_CHOICE_OPTION_NOT_FOUND)
  })

  test('Answer of a multiple choice question has to be among the options', () => {
    const exam = new Exam()
    const question1 = new QuestionMultipleChoice ('Pregunta multiple choice ', ['option 1', 'option 2', 'option 3'], 'option 2');
    exam.addQuestion(question1)

    expect( ()=> exam.answerQuestion(question1, 'invalid option'))
      .toThrow(Answer.ERROR_INVALID_RESPONSE)
    expect(exam.hasQuestions()).toBeTruthy()
    expect(exam.amountCorrectQuestions()).toBe(0)
  })
});
