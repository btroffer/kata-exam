import {Question} from "./Question";

export class Answer {
  static ERROR_INVALID_RESPONSE = 'Error invalid response'

  private _question: Question;
  private _response: string;

  constructor(question: Question, response: string) {
    if (!question.validResponse(response))
      throw Answer.ERROR_INVALID_RESPONSE

    this._question = question;
    this._response = response;
  }

  isCorrect() {
    return this._question.isCorrect(this._response)
  }
}
