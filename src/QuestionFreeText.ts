import {Question} from "./Question";

export class QuestionFreeText extends Question {

  constructor(statement: string, correctAnswer: string) {
    super(statement, correctAnswer);
  }

  validResponse(response: string): boolean {
    return true;
  }
}
