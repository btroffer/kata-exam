import {Question} from "./Question";
import {Answer} from "./answer";

export class Exam {
  private _questions: Question[];
  private _answers: any[];

  static ERROR_ANSWER_NOT_FOUND = 'Error answer not found';
  static ERROR_DUPLICATED_QUESTION = 'Error duplicated question';

  constructor() {
    this._questions = []
    this._answers = []
  }

  hasQuestions() {
    return this.amountQuestions() > 0
  }

  addQuestion(question: Question) {
    this.assertNotExistQuestion(question)

    this._questions.push(question)
  }

  amountQuestions() {
    return this._questions.length
  }

  amountCorrectQuestions() {
    return this._answers.filter((answer: Answer) => answer.isCorrect()).length
  }

  answerQuestion(question: Question, answer: string) {
    this.assertExistQuestion(question)

    this._answers.push(new Answer(question, answer))
  }

  private assertExistQuestion(questionToAssert: Question) {
    const answer = this._questions.find(question => question.isEqual(questionToAssert))
    if (answer === undefined)
      throw new Error(Exam.ERROR_ANSWER_NOT_FOUND)
  }

  private assertNotExistQuestion(questionToAssert: Question) {
    const answer = this._questions.find(question => question.isEqual(questionToAssert))
    if (answer !== undefined)
      throw new Error(Exam.ERROR_DUPLICATED_QUESTION)
  }
}
