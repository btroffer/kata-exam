export abstract class Question {
  protected _statement: string
  protected _correctAnswer: string

  protected constructor(statement: string, correctAnswer: string) {
    this._statement = statement
    this._correctAnswer = correctAnswer
  }

  get statement() {
    return this._statement
  }

  isEqual(question: Question) {
    return this.statement === question.statement
  }

  isCorrect(answer: String) {
    return this._correctAnswer === answer
  }

  abstract validResponse(response: string): boolean
}
