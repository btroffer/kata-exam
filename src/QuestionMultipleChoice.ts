import {Question} from "./Question";

export class QuestionMultipleChoice extends Question {
  static ERROR_MULTIPLE_CHOICE_OPTION_NOT_FOUND = 'Error multiple chpoce option not found';
  #options: string[];

  constructor(statement: string, options: string[], correctAnswer: string) {
    super(statement, correctAnswer);
    this.assertValidAnswer(options, correctAnswer)

    this.#options = options;
  }

  hasOption(response: string): boolean {
    return this.#options.find((option: string) => option === response) !== undefined;
  }

  validResponse(response: string): boolean {
    return this.hasOption(response)
  }

  private assertValidAnswer(options: string[], correctAnswer: string): void {
    const hasOption = options.find((option: string) => option === correctAnswer)
    if (hasOption === undefined)
      throw new Error(QuestionMultipleChoice.ERROR_MULTIPLE_CHOICE_OPTION_NOT_FOUND)
  }
}
