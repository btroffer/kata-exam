import type {Config} from '@jest/types';
import {defaults} from 'jest-config';

const jest: Config.InitialOptions = {
  moduleFileExtensions: [...defaults.moduleFileExtensions, 'ts'],
  reporters: ['default', 'jest-junit'],
  roots: ['<rootDir>/tests'],
  testEnvironment: 'jsdom',
};

export default jest;
