/* eslint-disable @typescript-eslint/no-var-requires */

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
require('dotenv').config();

const buildPath = path.resolve(__dirname, 'build');

module.exports = {
  mode: process.env['APP_MODE'],

  entry: {
    app: path.resolve(__dirname, 'src/index.ts'),
  },

  output: {
    path: buildPath,
    filename: '[name]-[chunkhash].js',
  },

  devServer: {
    static: buildPath,
  },

  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './src/index.html',
    }),
  ],

  optimization: {
    runtimeChunk: 'single',
  },

  resolve: {
    extensions: ['.ts', '.js'],
  },

  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
};
