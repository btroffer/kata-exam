# Enunciado

Un examen debe poder ser creado y contestado por cualquier usuario del sistema.

Cada usuario solo debe poder contestar un mismo examen una única vez.

Cada examen debe poder soportar una cantidad ilimitada de preguntas.

Cada examen debe poder ser contestado por muchos usuarios.

Cuando un usuario termina de contestar un examen, la app debe mostrarle el puntaje
que obtuvo; el cual está definido como la cantidad de preguntas correctas sobre el total
de preguntas.

El usuario evaluador debe poder ver todas las respuestas y el puntaje de cada examen
contestado.

Cada pregunta debe pertenecer a uno de los siguientes tres tipos:

* Multiple choice: Para este tipo de pregunta se deben definir las opciones
posibles así como cuál es la opción correcta. La pregunta se considera
contestada correctamente cuando la opción seleccionada es la opción que fue
definida como correcta.

* Verdadero / Falso: Para este tipo de pregunta se debe definir la respuesta
correcta como “verdadero” o como “falso”. La pregunta se considera contestada
correctamente cuando la respuesta seleccionada es igual a la que fue definida
como correcta.

* Texto libre: Para este tipo de pregunta se debe definir como respuesta correcta
un texto. La pregunta se considera contestada correctamente cuando el texto
ingresado es completamente igual al texto definido como “texto correcto”.

## TODO
* Agregar usuarios al examen
* Mejorar los tests
